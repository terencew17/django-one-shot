from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.
# Register the model using a class that inherits from admin.ModelAdmin
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "created_on",
        "id",
    )


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
        "is_completed",
        "id",
    )
